import * as vscode from 'vscode';
import * as path from 'path';


function main(editor: vscode.TextEditor | undefined) {
	if (editor === undefined) {
		vscode.window.showInformationMessage('git-log-range: no active editor');
		return;
	}
	const selection = editor.selection;
	const currentPath = editor.document.fileName;
	let term = vscode.window.createTerminal({
		name: 'git log -L',
		cwd: path.dirname(currentPath)
	});
	term.sendText(`git log -L ${selection.start.line},${selection.end.line}:${path.basename(currentPath)}`);
	term.show();
}

export function activate(context: vscode.ExtensionContext) {

	let disposable = vscode.commands.registerCommand('extension.gitlogrange', () => {
		main(vscode.window.activeTextEditor);
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
