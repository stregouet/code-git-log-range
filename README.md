# code-git-log-range

This extension provide vscode command (`git log range`) to easily launch `git log -L …` command
on selected text.